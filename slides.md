---
monaco: true
canvasWidth: 1200
---

# Slidev with Monaco

```python {monaco-diff}
def hello():
    return 'Hello world before'
~~~
def hello():
    return 'Hello world after'
```

| @slidev/cli     | slidev      | slidev build    | hosted                |
| :-------------- | :---------- | :-------------- | :-------------------- |
| ^0.46.3         | works       | broken [^1]     | iframe recursion [^2] |
| ^0.47           | broken [^3] | broken [^1]     | iframe recursion [^2] |
| ^0.48.0-beta.6  | works [^4]  | build fail [^5] | build fail [^5]       |
| ^0.48.0-beta.17 | works [^6]  | works [^6]      | works [^6]            |

[^1]: "GET /iframes/monaco/assets/index-z9jPtVTR.js HTTP/1.1" 404
[^2]: <Link :to="2">see infinite iframe recursion</Link>
[^3]: i don't know why
[^4]: fixed by [slidev#1308](https://github.com/slidevjs/slidev/pull/1308)
[^5]: infinite iframe folder recursion [build fail](https://gitlab.com/pinage404/slidev_with_monaco/-/jobs/6244339929#L66)
[^6]: fixed by [slidev#1342](https://github.com/slidevjs/slidev/pull/1342)

---

## infinite iframe recursion

`_redirect` contains

```txt
/slidev_with_monaco/*    /slidev_with_monaco/index.html   200
```

and iframe's asset are not found, so redirected to the index page

causing an infinite iframe recursion
